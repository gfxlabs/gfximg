module gitlab.com/gfxlabs/gfximg

go 1.17

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/srwiley/rasterx v0.0.0-20220128185129-2efea2b9ea41
	golang.org/x/image v0.0.0-20220412021310-99f80d0ecbab
	golang.org/x/net v0.0.0-20220412020605-290c469a71a5
)

require golang.org/x/text v0.3.7 // indirect
