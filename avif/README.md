# gfxavif [![Build Status](https://travis-ci.org/gfxlabs/gfxavif.svg?branch=master)](https://travis-ci.org/gfxlabs/gfxavif) [![GoDoc](https://godoc.org/github.com/gfxlabs/gfxavif?status.svg)](https://godoc.org/github.com/gfxlabs/gfxavif)

gfxavif implements
AVIF ([AV1 Still Image File Format](https://aomediacodec.github.io/av1-avif/))
encoder for Go using libaom, the [high quality](https://github.com/gfxlabs/av1-bench)
AV1 codec.

## Requirements

Make sure libaom is installed.

debian: 
```bash
sudo apt-get install libaom-dev
```

alpine
```bash
apk add aom aom-dev build-base
```

## Usage

To use gfxavif in your Go code:

```go
import "gitlab.com/gfxlabs/gfxavif"
```

For further details see [GoDoc documentation](https://godoc.org/gitlab.com/gfxlabs/gfxavif).

## Example

```go
package main

import (
	"image"
	_ "image/jpeg"
	"log"
	"os"

	"gitlab.com/gfxlabs/gfxavif"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Usage: %s src.jpg dst.avif", os.Args[0])
	}

	srcPath := os.Args[1]
	src, err := os.Open(srcPath)
	if err != nil {
		log.Fatalf("Can't open sorce file: %v", err)
	}

	dstPath := os.Args[2]
	dst, err := os.Create(dstPath)
	if err != nil {
		log.Fatalf("Can't create destination file: %v", err)
	}

	img, _, err := image.Decode(src)
	if err != nil {
		log.Fatalf("Can't decode source file: %v", err)
	}

	err = avif.Encode(dst, img, nil)
	if err != nil {
		log.Fatalf("Can't encode source image: %v", err)
	}

	log.Printf("Encoded AVIF at %s", dstPath)
}
```

## CLI

gfxavif comes with handy CLI utility `avif`. It's available as a docker image gfxlabs/gfxavif:latest


```bash

docker run -it gfxlabs/gfxavif:latest -v ... /bin/sh

#once in environment with volume bounded

# Encode JPEG to AVIF with default settings
/src/cmd.exe -e cat.jpg -o kitty.avif

# Encode PNG with slowest speed
/src/cmd.exe -e dog.png -o doggy.avif --best -q 15

# Lossless encoding
/src/cmd.exe -e pig.png -o piggy.avif --lossless

# Show help
/src/cmd.exe -h
```

## License

gfxavif is licensed under [CC0](COPYING).
